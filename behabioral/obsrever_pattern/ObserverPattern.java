import java.util.ArrayList;
import java.util.Iterator;
import java.util.*;
import java.io.*;

/**
 * name : Observer pattern, type : Behavioural, description : An object called
 * Subject (observable) maintains a list of it's dependents, called Observers,
 * and notifies them upon state change of Subject by calling one of their
 * methods.
 * 
 */

public class ObserverPattern {
    public static void main(String[] args) throws Exception {
        System.out.println("Observer pattern : ");
        AlienObservatory observatory = new AlienObservatory();
        CNN cnn = new CNN(observatory);
        CBS cbs = new CBS(observatory);
        ScienceChannel scn = new ScienceChannel(observatory);
        observatory.add(cnn);
        observatory.add(cbs);
        observatory.add(scn);

        observatory.Run();
    }
}

class AlienObservatory implements IObservable {

    private List<IObserver> observers;

    private int alienCount;

    public AlienObservatory() {
        this.observers = new ArrayList<>();
        alienCount = 0;
    }

    public void add(IObserver obj) {
        this.observers.add(obj);
    }

    public void remove(IObserver obj) {
        this.observers.remove(obj);
    }

    public void notifyClients() {
        Iterator<IObserver> itr = this.observers.iterator();
        while (itr.hasNext()) {
            IObserver next = itr.next();
            next.update();
        }
    }

    public String getAlienNews() {
        return String.valueOf(this.alienCount) + " Alien found!";
    }

    public void Run() throws Exception {
        for (int i = 0; i < 10; i++) {
            this.alienCount += 1;
            notifyClients();
            Thread.sleep(2000);
        }
    }

}

class CNN implements IObserver {
    private AlienObservatory observatory;

    public CNN(AlienObservatory observable) {
        this.observatory = observable;
    }

    public void update() {
        String latestNews = observatory.getAlienNews();
        System.out.println("CNN BREAKING news on alien observation : " + latestNews);
    }
}

class CBS implements IObserver {
    private AlienObservatory alienObservatory;

    public CBS(AlienObservatory observatory) {
        this.alienObservatory = observatory;
    }

    public void update() {
        String latestNews = alienObservatory.getAlienNews();
        System.out.println("CBS broadcasting alien news : " + latestNews);
    }
}

class ScienceChannel implements IObserver {
    private AlienObservatory alienObservatory;

    public ScienceChannel(AlienObservatory alienObservatory) {
        this.alienObservatory = alienObservatory;
    }

    public void update() {
        String latestNews = alienObservatory.getAlienNews();
        System.out.println("Science Channel : " + latestNews);
    }
}
