/**
 * Program to an interface not an implementation Strategy pattern: A class
 * behaviour or algorithm can be changed at run time. This is a type of
 * behaviour pattern.
 * 
 * 
 */

public class StrategyPattern {
    public static void main(String[] args) {

        // fast eating, deep sleeping and street fighting dog
        Dog dog1 = new Dog(new FastEating(), new DeepSleeping(), new StreetFighting());
        dog1.ShowEating();
        dog1.ShowSleeping();
        dog1.ShowFighting();

        // slow eating, not sleeping and street fighting dog
        Dog dog2 = new Dog(new SlowEating(), new NotSleeping(), new StreetFighting());

        dog2.ShowEating();
        dog2.ShowSleeping();
        dog2.ShowFighting();
    }
}

class Dog {
    private IEatingStrategy eating;
    private ISleepingStrategy sleeping;
    private IFightingStrategy fighting;

    public Dog(IEatingStrategy eating, ISleepingStrategy sleeping, IFightingStrategy fighting) {
        this.eating = eating;
        this.sleeping = sleeping;
        this.fighting = fighting;
    }

    public void ShowEating() {
        System.out.println(eating.eat());
    }

    public void ShowSleeping() {
        System.out.println(sleeping.sleep());
    }

    public void ShowFighting() {
        System.out.println(fighting.fight());
    }
}

class FastEating implements IEatingStrategy {
    public String eat() {
        return "eating fast";
    }
}

class SlowEating implements IEatingStrategy {
    public String eat() {
        return "eating slow";
    }
}

class DeepSleeping implements ISleepingStrategy {
    public String sleep() {
        return "Sleeping deep.";
    }
}

class NotSleeping implements ISleepingStrategy {
    public String sleep() {
        return "Not sleeping";
    }
}

class StreetFighting implements IFightingStrategy {
    public String fight() {
        return "Street fighting";
    }
}
