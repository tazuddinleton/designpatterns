public interface ISleepingStrategy {
    String sleep();
}