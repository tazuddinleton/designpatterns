public interface IEatingStrategy {
    String eat();
}