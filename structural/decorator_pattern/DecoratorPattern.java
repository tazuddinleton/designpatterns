/**
 * Decorator pattern: Attach additional responsibility to a class dynamically.
 * Decorators provide flexible alternative to sublclassing for extending
 * functionality.
 */

public class DecoratorPattern {
    public static void main(String[] args) {

        Beverage myStyle = new ChocolateDecorator(new MochaDecorator(new Espresso()));

        System.out.println(myStyle.getCost());
        System.out.println(myStyle.getDescription());

        Beverage crazzy = new ChocolateDecorator(
                new ChocolateDecorator(new MochaDecorator(new ChocolateDecorator(new Decaf()))));

        System.out.println(crazzy.getCost());
        System.out.println(crazzy.getDescription());

    }
}

class MochaDecorator extends Beverage {
    private Beverage beverage;
    private int cost;

    public MochaDecorator(Beverage beverage) {
        this.beverage = beverage;
        this.cost = 20;
    }

    @Override
    public int getCost() {
        return this.beverage.getCost() + this.cost;
    }

    @Override
    public String getDescription() {
        return this.beverage.getDescription() + " with Mocha";
    }
}

class ChocolateDecorator extends Beverage {
    private Beverage beverage;
    private int cost;

    public ChocolateDecorator(Beverage beverage) {
        this.cost = 30;
        this.beverage = beverage;
    }

    @Override
    public int getCost() {
        return beverage.getCost() + this.cost;
    }

    @Override
    public String getDescription() {
        return this.beverage.getDescription() + " with Chocolate";
    }
}

class Decaf extends Beverage {
    private int cost;

    public Decaf() {
        this.cost = 20;
    }

    @Override
    public int getCost() {
        return this.cost;
    }

    @Override
    public String getDescription() {
        return "Decaf: ";
    }
}

class Espresso extends Beverage {
    private int cost;

    public Espresso() {
        this.cost = 40;
    }

    @Override
    public int getCost() {
        return this.cost;
    }

    @Override
    public String getDescription() {
        return "Espresso:";
    }
}

abstract class Beverage {
    private int cost;

    public abstract int getCost();

    public abstract String getDescription();
}
